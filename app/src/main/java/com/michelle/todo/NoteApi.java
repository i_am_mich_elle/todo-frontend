package com.michelle.todo;

import android.annotation.SuppressLint;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import timber.log.Timber;

public class NoteApi {
    private OkHttpClient client = new OkHttpClient();
    private Gson gson = new Gson();
    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");

    public List<Note> GetNotes(){
        Request request = new Request.Builder()
                .url("https://mighty-castle-58154.herokuapp.com/api/todos")
                .build();
        try {
            Response response = client.newCall(request).execute();
            String json = response.body().toString();
            Type getListType = new TypeToken<List<Note>>() {
            }.getType();
            return gson.fromJson(json, getListType);

        } catch (IOException ex) {

        }
        return new ArrayList<>();
    }

    Note PostNote (String json) {
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url("https://mighty-castle-58154.herokuapp.com/api/todos")
                .post(body)
                .build();
        try {
            Response response = client.newCall(request).execute();
            String postResponse = response.body().toString();
            return gson.fromJson(postResponse, Note.class);


        } catch (IOException ex) {

        }
        return null;
    }

    Note PutNote(String json) {
        RequestBody body = RequestBody.create(JSON, json);
        Request postRequest = new Request.Builder()
                .url("https://mighty-castle-58154.herokuapp.com/api/todos/%s")
                .put(body)
                .build();

        try {
            Response response = client.newCall(postRequest).execute();
            String postResponse = response.body().toString();
            return gson.fromJson(postResponse, Note.class);

        } catch (IOException ex){

        }
        return null;
    }

    Note DeleteNote(String id) {
        Request request = new Request.Builder()
                .url(String.format("https://mighty-castle-58154.herokuapp.com/api/todos/%s", id))
                .delete()
                .build();

        try {
            Response response = client.newCall(request).execute();
            String deleteResponce = response.body().toString();
            return gson.fromJson(deleteResponce, Note.class);

        } catch (IOException ex) {

        }
        return null;
    }
}



