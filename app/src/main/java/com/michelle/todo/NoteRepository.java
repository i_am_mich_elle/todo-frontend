package com.michelle.todo;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

import timber.log.Timber;


public class NoteRepository {
    private NoteDao noteDao;
    private LiveData<List<Note>> allNotes;
    private NoteApi noteApi = new NoteApi();

    public  NoteRepository(Application application){
        NoteDatabase database = NoteDatabase.getInstance(application);
        noteDao = database.noteDao();
        noteApi = new NoteApi();
        allNotes = (LiveData<List<Note>>) noteApi.GetNotes();
    }


    public void insert(Note note){
        new InsertNoteAsyncTask(noteApi).execute(note);
    }

    public void update(Note note){
        new UpdateNoteAsyncTask(noteApi).execute(note);
    }

    public void delete(Note note){
        new DeleteNoteAsyncTask(noteApi).execute(note);
    }

    public void deleteAllNotes(){
        new DeleteNoteAsyncTask(noteApi).execute();
    }

    public LiveData<List<Note>> getAllNotes(){
        return allNotes;
    }

    private static class InsertNoteAsyncTask extends AsyncTask<Note, Void, Void> {
        private NoteApi noteApi = new NoteApi();
        private InsertNoteAsyncTask(NoteApi noteApi) {
            this.noteApi = noteApi;
        }

        @Override
        protected Void doInBackground(Note... notes) {
            noteApi.PostNote("");
            Timber.i("PostNote");
            return null;
        }
    }

    private static class UpdateNoteAsyncTask extends AsyncTask<Note, Void, Void> {
        NoteApi noteApi = new NoteApi();
        private UpdateNoteAsyncTask(NoteApi noteApi) {
            this.noteApi = noteApi;
        }

        @Override
        protected Void doInBackground(Note... notes) {
            noteApi.PutNote("");
            return null;
        }
    }

    private static class DeleteNoteAsyncTask extends AsyncTask<Note, Void, Void> {
        NoteApi noteApi = new NoteApi();
        private DeleteNoteAsyncTask(NoteApi noteApi) {
            this.noteApi = noteApi;
        }

        @Override
        protected Void doInBackground(Note... notes) {
            Note note =notes[0];
            noteApi.DeleteNote(""+note.getId());
            return null;
        }
    }

    private static class DeleteAllNoteAsyncTask extends AsyncTask<Void, Void, Void> {
        private NoteDao noteDao;
        NoteApi noteApi = new NoteApi();

        private DeleteAllNoteAsyncTask(NoteDao noteDao) {
            this.noteDao = noteDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            noteDao.deleteAllNotes();
            return null;
        }
    }

}
